<?php 
$I = new FunctionalTester($scenario);
$I->am('a Larabook member');
$I->wantTo('post status to my profile.');

$I->signIn();

$I->amOnPage('statuses');

$status = 'My first post!';

$I->postAStatus($status);

$I->seeRecord('statuses', [
	'body' => $status,
	'user_id' => Auth::user()->id,
]);

$I->seeCurrentUrlEquals('/statuses');
$I->see($status);
