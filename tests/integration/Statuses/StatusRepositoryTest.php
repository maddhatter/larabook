<?php

use Larabook\Statuses\StatusRepository;
use Laracasts\TestDummy\Factory as TestDummy;

class StatusRepositoryTest extends \Codeception\TestCase\Test
{

    /**
     * @var \IntegrationTester
     */
    protected $tester;

    /**
     * @var \Larabook\Statuses\StatusRepository
     */
    protected $repo;

    protected function _before()
    {
        $this->repo = new StatusRepository();
    }

    /** @test * */
    public function it_gets_all_statuses_for_a_user()
    {
        // given i have 2 users
        $users = TestDummy::times(2)->create('Larabook\Users\User');

        // Create statuses for both
        TestDummy::times(2)->create('Larabook\Statuses\Status', [
            'user_id' => $users[0]->id,
            'body' => 'My status',
        ]);

        TestDummy::times(2)->create('Larabook\Statuses\Status', [
            'user_id' => $users[1]->id,
            'body' => 'Someone other status',
        ]);

        //When I fetch statuses for 1 user
        $statusesForUser = $this->repo->getAllForUser($users[0]);

        // Then I should only get that user's statuses
        $this->assertCount(2, $statusesForUser);
        $this->assertEquals('My status', $statusesForUser[0]->body);
        $this->assertEquals('My status', $statusesForUser[1]->body);
    }

    /** @test * */
    public function it_saves_a_status_for_a_user()
    {
        // Given I have an unsaved status
        $status = TestDummy::build('Larabook\Statuses\Status', [
            'user_id' => null,
            'body' => 'Saved Status',
        ]);

        // And an existing user
        $user = TestDummy::create('Larabook\Users\User');

        // When I try to persist the status
        $this->repo->save($status, $user->id);

        // It should be saved w/ the correct user id
        $this->tester->seeRecord('statuses', [
            'user_id' => $user->id,
            'body' => 'Saved Status',
        ]);
    }
}
