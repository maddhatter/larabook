<?php

class DatabaseSeeder extends Seeder {

    /**
     * Tables to clear before seeding
     *
     * @var array
     */
    protected $tables = [
        'users',
        'statuses',
    ];

    /**
     * Seeders to run
     *
     * @var array
     */
    protected $seeders = [
        'UsersTableSeeder',
        'StatusesTableSeeder',
    ];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->cleanDatabase();

		foreach ($this->seeders as $seed) {
            $this->call($seed);
        }
	}

    /**
     * Trunc tables
     */
    private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
