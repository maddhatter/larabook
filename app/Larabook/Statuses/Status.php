<?php namespace Larabook\Statuses;

use Eloquent;
use Laracasts\Commander\Events\EventGenerator;
use Larabook\Statuses\Events\StatusWasPublished;
use Laracasts\Presenter\PresentableTrait;

class Status extends Eloquent {

	use EventGenerator, PresentableTrait;
	
	/**
	 * Fillable fields for a new status
	 * @var array
	 */
	protected $fillable = ['body'];

    /**
     * The path to the presenter
     * @var string
     */
    protected $presenter = 'Larabook\Statuses\StatusPresenter';

	/**
	 * Publish a new status
	 * @param  string $body
	 * @return static
	 */
	public static function publish($body)
	{
		$status = new static(compact('body'));

		$status->raise(new StatusWasPublished($body));

		return $status;
	}

	/**
	 * A status belongs to a user
	 * @return mixed
	 */
	public function user()
	{
		return $this->belongsTo('Larabook\Users\User');
	}

}
