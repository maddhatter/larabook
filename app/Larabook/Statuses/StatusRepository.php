<?php namespace Larabook\Statuses;

use Larabook\Users\User;

class StatusRepository {

	/**
	 * Save a new status
	 * @param  Status $status [description]
	 * @param  int $userId
	 * @return mixed
	 */
	public function save(Status $status, $userId)
	{
		
		return User::findOrFail($userId)
			->statuses()
			->save($status);
	
	}

    /**
     * @param User $user
     * @return mixed
     */
    public function getAllForUser(User $user)
	{
		return $user->statuses()->with('user')->latest()->get();
	}
}
