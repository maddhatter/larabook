<?php namespace Larabook\Registration;

class RegisterUserCommand {
	
	public $username;
	public $email;
	public $password;

	public function __construct($username, $email, $password)
	{
		$this->email = $email;
		$this->username = $username;
		$this->password = $password;
	}

}
