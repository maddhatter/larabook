<?php namespace Larabook\Forms;

use Laracasts\Validation\FormValidator;

class SignInForm extends FormValidator {
	
	protected $rules = [
		'email' => 'email|required',
		'password' => 'required',
	];

}